package com.mda.coordinatetracker.receivers;

import timber.log.Timber;

/**
 * User: mda
 * Date: 4/3/13
 * Time: 10:21 AM
 */
public abstract class AbstractErrorListener implements ErrorReceiver.ErrorListener {
    public static String TAG = AbstractErrorListener.class.getName();
    @Override
    public void onLocationError() {
        Timber.tag(TAG).e("LocationError");
    }

    @Override
    public void onNetworkError() {
        Timber.tag(TAG).e("NetworkError");
    }
}
