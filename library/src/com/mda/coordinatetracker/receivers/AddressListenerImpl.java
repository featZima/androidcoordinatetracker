package com.mda.coordinatetracker.receivers;

import android.location.Location;
import timber.log.Timber;

/**
 * User: mda
 * Date: 4/3/13
 * Time: 10:21 AM
 */
public class AddressListenerImpl extends AbstractErrorListener implements AddressReceiver.AddressListener {
    @Override
    public void onAddressRetrieved(String address, Location location) {
    }
}
