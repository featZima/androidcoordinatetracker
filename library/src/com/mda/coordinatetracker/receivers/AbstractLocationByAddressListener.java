package com.mda.coordinatetracker.receivers;

import timber.log.Timber;

/**
 * User: mda
 * Date: 4/3/13
 * Time: 10:21 AM
 */
public abstract class AbstractLocationByAddressListener extends AbstractErrorListener implements LocationByAddressReceiver.LocationByAddressListener {
    @Override
    public void onLocationRetrieved(double lat, double lng) {
        Timber.tag(TAG).d("Location: lat = " + lat + ", lng = " + lng);
    }
}
